package csi_vesit.csivesit.Minigame;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import csi_vesit.csivesit.Mails.Mail;
import csi_vesit.csivesit.R;

/**
 * Created by all on 31-Jan-17.
 */
public class ScoreAdapter extends ArrayAdapter {

    List list = new ArrayList();

    public ScoreAdapter(Context context, int resource) {
        super(context, resource);
    }

    @Override
    public void add(Object object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        ScoreHolder scoreHolder;
        if (row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.score_row,parent,false);
            scoreHolder = new ScoreHolder();
            scoreHolder.game_no = (TextView) row.findViewById(R.id.game_no);
            scoreHolder.desk_no = (TextView) row.findViewById(R.id.desk_no);
            scoreHolder.score = (TextView) row.findViewById(R.id.score);
            row.setTag(scoreHolder);
        }else{
            scoreHolder = (ScoreHolder) row.getTag();
        }
        Score score = (Score) this.getItem(position);
        if(score.getDesk_no() == 0){
            scoreHolder.game_no.setText("Wheel of Fortune set, next game should be Game" + score.getGame_no());
            scoreHolder.desk_no.setText("");
            scoreHolder.score.setText("");
            return row;
        }else {
            scoreHolder.game_no.setText("Game: " + score.getGame_no());
            scoreHolder.desk_no.setText("Desk: " + score.getDesk_no());
            scoreHolder.score.setText("Score: " + score.getScore());
            return row;
        }

    }

    static class ScoreHolder{
        TextView game_no,desk_no,score;
    }

}
