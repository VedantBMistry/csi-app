package csi_vesit.csivesit.Minigame;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import csi_vesit.csivesit.R;

public class Auth extends AppCompatActivity {

    EditText et_ip,et_team_no,et_pass;
    Button submit;
    private Toolbar toolbar;
    private String TAG = "CSIVESIT";
    private ImageView wait;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        toolbar = (Toolbar)findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        wait = (ImageView) findViewById(R.id.wait);

        try{
            SharedPreferences scoreside = getApplicationContext().getSharedPreferences("Minigame", Context.MODE_PRIVATE);
            final String ip_shared = scoreside.getString("IPadd","");
            final String team_shared = scoreside.getString("Team_No","");
            final String pass_shared = scoreside.getString("Password","");

            et_ip = (EditText)findViewById(R.id.et_ip);
            et_team_no = (EditText)findViewById(R.id.et_team_no);
            et_pass = (EditText)findViewById(R.id.et_pass);
            submit = (Button) findViewById(R.id.submit);


            et_ip.setText(ip_shared);
            et_team_no.setText(team_shared);
            et_pass.setText(pass_shared);
        }catch (Exception e){}

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!et_ip.getText().toString().trim().equals("") && !et_team_no.getText().toString().trim().equals("") && !et_pass.getText().toString().trim().equals("")) {
                        Log.i(TAG,"here0");
                        final Animation animRotate = AnimationUtils.loadAnimation(Auth.this,R.anim.rotate);

                        wait.setVisibility(View.VISIBLE);
                        wait.startAnimation(animRotate);

                        Background background = new Background(Auth.this);
                        background.execute(et_ip.getText().toString().trim(), et_team_no.getText().toString().trim(), et_pass.getText().toString().trim());
                    }
                }catch (Exception e){
                    Log.i(TAG,String.valueOf(e));
                }
            }
        });
    }

    class Background extends AsyncTask<String, Void, String>{

        Context context;
        public Background(Context context){this.context = context;}

        @Override
        protected void onPostExecute(String s) {
            wait.setVisibility(View.INVISIBLE);
            try {

                if (s.equals("yes")) {
                    SharedPreferences scoreside = getApplicationContext().getSharedPreferences("Minigame", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = scoreside.edit();
                    editor.putString("IPadd",et_ip.getText().toString().trim());
                    editor.putString("Team_No",et_team_no.getText().toString().trim());
                    editor.putString("Password",et_pass.getText().toString());
                    editor.commit();
                    Intent i = new Intent(Auth.this, csi_vesit.csivesit.Minigame.Display.class);
                    Log.i(TAG,"here2");
                    i.putExtra("team_no", Integer.parseInt(et_team_no.getText().toString().trim()));
                    i.putExtra("ip", et_ip.getText().toString().trim());
                    startActivity(i);
                    finish();
                }else if (s.equals("no")){
                    Toast.makeText(getApplicationContext(),"Incorrect Password",Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e){
                Log.i(TAG,String.valueOf(e));
                Toast.makeText(Auth.this,"Network error.",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... params) {

            String Url = "http://"+params[0]+"/CsiScore/Auth.php";
            try {
                URL url = new URL(Url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setRequestMethod("POST");

                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data = URLEncoder.encode("team_no","UTF-8")+"="+URLEncoder.encode(params[1],"UTF-8")+"&"+
                        URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(params[2],"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                Log.i(TAG,"here");
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String line = "";
                String result = "";
                while ((line = bufferedReader.readLine()) != null){
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }
    }
}
