package csi_vesit.csivesit.Minigame;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import csi_vesit.csivesit.R;

public class Display extends AppCompatActivity {

    TextView game1,game2,game3,game4,game5,game6,total,teamno;
    int totalScoe = 0;
    String ip;
    int team;
    private Toolbar toolbar;
    JSONObject jsonObject;
    JSONArray jsonArray;
    ListView listView;
    ScoreAdapter scoreAdapter;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.minigamemenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_refresh:
                Intent intent = new Intent(this,Display.class);
                intent.putExtra("ip",ip);
                intent.putExtra("team_no",team);
                startActivity(intent);
                finish();
                break;

        }
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        toolbar = (Toolbar)findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);

        ip = getIntent().getExtras().getString("ip");
        team = getIntent().getExtras().getInt("team_no");

        listView = (ListView) findViewById(R.id.listView);
        scoreAdapter = new ScoreAdapter(this,R.layout.score_row);
        listView.setAdapter(scoreAdapter);
        total = (TextView)findViewById(R.id.total);
        teamno = (TextView)findViewById(R.id.teamno);
        teamno.setText("Team No: "+team);

        Background background = new Background(this);
        background.execute(String.valueOf(team));

    }


    class Background extends AsyncTask<String,Void,String>{
        Context context;
        public Background(Context context) {
            this.context = context;
        }

        @Override
        protected void onPostExecute(String s) {
            try{
                jsonObject = new JSONObject(s);
                jsonArray = jsonObject.getJSONArray("response");
                int count = 0;
                while (count < jsonArray.length()) {

                    JSONObject JO = jsonArray.getJSONObject(count);
                    String team_no = JO.getString("team_no");
                    String game_no = JO.getString("game_no");
                    String desk_no = JO.getString("desk_no");
                    String score = JO.getString("score");


                    totalScoe += Integer.parseInt(score);

                    Score score1 = new Score(Integer.parseInt(game_no), Integer.parseInt(desk_no), Integer.parseInt(score));
                    scoreAdapter.add(score1);
                    count++;
                }
                total.setText("Total Score: "+totalScoe);

            }catch (Exception e){
                Log.i("CSIVESIT",String.valueOf(e));
                Toast.makeText(Display.this,"Something went wrong! try again.",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String Url = "http://"+ip+"/CsiScore/csifetch.php";
            try {
                URL url = new URL(Url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String post_data = URLEncoder.encode("team_no","UTF-8")+"="+URLEncoder.encode(params[0],"UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String line = "";
                String result = "";
                while ((line = bufferedReader.readLine()) != null){
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
