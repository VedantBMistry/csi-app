package csi_vesit.csivesit.Minigame;

/**
 * Created by all on 31-Jan-17.
 */
public class Score {
    private int game_no,desk_no,score;

    public Score(int game_no, int desk_no, int score) {
        this.game_no = game_no;
        this.desk_no = desk_no;
        this.score = score;
    }

    public int getGame_no() {
        return game_no;
    }

    public int getDesk_no() {
        return desk_no;
    }

    public int getScore() {
        return score;
    }
}
