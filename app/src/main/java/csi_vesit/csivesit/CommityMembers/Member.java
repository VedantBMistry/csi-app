package csi_vesit.csivesit.CommityMembers;

/**
 * Created by all on 22-Jan-17.
 */
public class Member {

    public int id;
    String name,post;

    public Member(int id, String name, String post) {
        this.id = id;
        this.name = name;
        this.post = post;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPost() {
        return post;
    }
}
