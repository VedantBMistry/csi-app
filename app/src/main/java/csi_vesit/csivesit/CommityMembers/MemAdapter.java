package csi_vesit.csivesit.CommityMembers;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import csi_vesit.csivesit.R;



public class MemAdapter extends RecyclerView.Adapter<MemAdapter.MemHolder>{






    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    Context ctx;
    ArrayList<Member> arrayList = new ArrayList<>();
    public MemAdapter(ArrayList<Member> arrayList,Context context){
        this.arrayList = arrayList;
        ctx = context;
    }


    @Override
    public MemAdapter.MemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.com_memb,parent,false);
        MemHolder memHolder = new MemHolder(view);
        return memHolder;
    }

    @Override
    public void onBindViewHolder(MemAdapter.MemHolder holder, int position) {
        final Member member = arrayList.get(position);
        holder.iv.setImageBitmap(decodeSampledBitmapFromResource(ctx.getResources(),member.getId(),70,70));
        holder.t1.setText(member.getName());
        holder.t2.setText(member.getPost());

        holder.iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ctx,ProfilePic.class);
                i.putExtra("propic",member.getId());
                i.putExtra("nameof",member.getName());
                i.putExtra("postof",member.getPost());
                ctx.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MemHolder extends RecyclerView.ViewHolder{

        ImageView iv;
        TextView t1,t2;

        public MemHolder(View view) {
            super(view);
            t1 = (TextView)view.findViewById(R.id.name);
            t2 = (TextView)view.findViewById(R.id.post);
            iv = (ImageView)view.findViewById(R.id.imageView);
        }
    }
}
