package csi_vesit.csivesit.CommityMembers;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import java.util.ArrayList;

import csi_vesit.csivesit.R;

public class ComMembers extends AppCompatActivity {

    RecyclerView listView;
    MemAdapter memAdapter;
    DatabaseOpener myDb;
    private Toolbar toolbar;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Member> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_com_members);

        toolbar = (Toolbar)findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (RecyclerView) findViewById(R.id.listView);
        layoutManager = new LinearLayoutManager(this);
        listView.setLayoutManager(layoutManager);
        listView.setHasFixedSize(true);
        listView.setAdapter(memAdapter);
        memAdapter = new MemAdapter(arrayList,this);
        myDb = new DatabaseOpener(this);

        Cursor res = myDb.showMember();
        while(res.moveToNext()){
            String name = res.getString(0);
            String post = res.getString(1);
            int id = res.getInt(2);
            Member member = new Member(id,name,post);
            arrayList.add(member);
        }

        listView.setAdapter(memAdapter);
    }
}
