package csi_vesit.csivesit.CommityMembers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by all on 22-Jan-17.
 */
public class DatabaseOpener extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "CSIVESIT";
    public static final String TABLE_NAME = "COMM_MEM";
    public static final String MEMNAME = "MEM_NAME";
    public static final String MEMPOST = "MEM_POST";
    public static final String MEMIMAGE = "MEM_IMAGE";


    public static final String TABLE_NAME2 = "SERVERKEY";
    public static final String ISSET = "SEET";

    public DatabaseOpener(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + "(MEM_NAME TEXT, MEM_POST TEXT, MEM_IMAGE INT)");
        db.execSQL("CREATE TABLE " + TABLE_NAME2 + "(SEET INT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME2);
        onCreate(db);
    }

    public boolean insertMember(String name,String post, int id){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MEMNAME,name);
        contentValues.put(MEMPOST,post);
        contentValues.put(MEMIMAGE,id);
        long result =  db.insert(TABLE_NAME,null,contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public boolean insertEntry(int id){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ISSET,id);
        long result =  db.insert(TABLE_NAME2,null,contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public Cursor showMember(){
        SQLiteDatabase db = getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME,null);
        return res;
    }

    public Cursor showEntry(){
        SQLiteDatabase db = getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME2,null);
        return res;
    }

}
