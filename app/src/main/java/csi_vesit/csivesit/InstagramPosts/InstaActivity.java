package csi_vesit.csivesit.InstagramPosts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import csi_vesit.csivesit.R;

public class InstaActivity extends AppCompatActivity {

    WebView web;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insta);
        toolbar = (Toolbar) findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);
        web= (WebView) findViewById(R.id.webView);
        WebSettings ws=web.getSettings();
        ws.setJavaScriptEnabled(true);
        web.loadUrl("https://www.instagram.com/csi_vesit/");
        web.setWebViewClient(new WebViewClient());

    }

    @Override
    public void onBackPressed() {
        if(web.canGoBack()){
            web.goBack();
        }
        else{
            super.onBackPressed();
        }
    }
}
