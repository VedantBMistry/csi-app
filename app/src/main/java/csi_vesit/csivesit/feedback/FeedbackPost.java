package csi_vesit.csivesit.feedback;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import csi_vesit.csivesit.startpage.MainActivity;
import csi_vesit.csivesit.R;

public class FeedbackPost extends AppCompatActivity {

    EditText name,abt,msg;
    ImageButton imageButton;
    Spinner spinner;
    String type = "";
    private Toolbar toolbar;
    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_post);

        toolbar = (Toolbar)findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        name = (EditText)findViewById(R.id.name);
        abt = (EditText)findViewById(R.id.abt);
        msg = (EditText)findViewById(R.id.msg);

        imageButton = (ImageButton)findViewById(R.id.imageButton);
        spinner = (Spinner)findViewById(R.id.spinner);

        adapter = ArrayAdapter.createFromResource(this,R.array.type,android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!name.getText().toString().trim().equals("") && !abt.getText().toString().trim().equals("") && !msg.getText().toString().trim().equals("")){
                    Background background = new Background(FeedbackPost.this);
                    background.execute(name.getText().toString().trim(),type,abt.getText().toString().trim(),msg.getText().toString().trim());
                }
            }
        });

    }


    class Background extends AsyncTask<String, Void, String>{

        Context context;

        public Background(Context context) {
            this.context = context;
        }

        @Override
        protected void onPostExecute(String s) {
            try{
                if(s.equals("yes")) {
                    Toast.makeText(context, "Feedback submitted.", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(context, MainActivity.class);
                    startActivity(i);
                    finish();
                }else{
                    Toast.makeText(context,"Network Error!",Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e){
                Toast.makeText(context,"Network Error!",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                URL url = new URL("http://businessworld.000webhostapp.com/csi/feedback.php");
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String post_data = URLEncoder.encode("name","UTF-8")+"="+URLEncoder.encode(params[0],"UTF-8")+"&"+
                                URLEncoder.encode("type","UTF-8")+"="+URLEncoder.encode(params[1],"UTF-8")+"&"+
                                URLEncoder.encode("abt","UTF-8")+"="+URLEncoder.encode(params[2],"UTF-8")+"&"+
                                URLEncoder.encode("msg","UTF-8")+"="+URLEncoder.encode(params[3],"UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result = "";
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
