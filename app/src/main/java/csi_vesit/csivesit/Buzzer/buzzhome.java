package csi_vesit.csivesit.Buzzer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import csi_vesit.csivesit.R;

public class buzzhome extends AppCompatActivity {

    Button go;
    EditText et_ip,et_team_no;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buzzhome);

        toolbar = (Toolbar)findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        go = (Button)findViewById(R.id.go);
        et_ip = (EditText) findViewById(R.id.et_ip);
        et_team_no = (EditText)findViewById(R.id.et_team_no);

        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!et_ip.getText().toString().trim().equals("") && !et_team_no.getText().toString().trim().equals("")){
                    Intent in = new Intent(buzzhome.this,buzz.class);
                    in.putExtra("ip",et_ip.getText().toString().trim());
                    in.putExtra("team",Integer.parseInt(et_team_no.getText().toString().trim()));
                    startActivity(in);
                }
            }
        });

    }
}
