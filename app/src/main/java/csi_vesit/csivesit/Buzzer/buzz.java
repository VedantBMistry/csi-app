package csi_vesit.csivesit.Buzzer;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import csi_vesit.csivesit.R;

public class buzz extends AppCompatActivity {

    private Socket socket;
    ImageButton myButton;
    TextView text;
    private static final int SERVER_PORT = 8888;
    Thread clientThread;
    Handler handler;
    private Toolbar toolbar;
    private String SERVER_IP;
    int i;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buzz);

        toolbar = (Toolbar)findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);

        i = getIntent().getExtras().getInt("team");
        myButton = (ImageButton)findViewById(R.id.myButton);
        text = (TextView)findViewById(R.id.text);

        handler = new Handler();
        SERVER_IP = getIntent().getExtras().getString("ip");



        this.clientThread = new Thread(new ClientThread());
        this.clientThread.start();

        //this.serverThread = new Thread(new ServerThread());
        //this.serverThread.start();


        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = "Team No: " + String.valueOf(i);
                handler.post(new updateUIThread(text));
                try {
                    if (socket.isConnected()) {
                        PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                        Toast.makeText(getApplicationContext(), "Buzz", Toast.LENGTH_SHORT).show();
                        out.println(text);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e){
                    Toast.makeText(getApplicationContext(),"Not Connected to Server",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    class ClientThread implements Runnable{
        @Override
        public void run() {
            try {
                InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
                socket = new Socket(serverAddr,SERVER_PORT);
                CommunicationThread commThread = new CommunicationThread(socket);
                new Thread(commThread).start();


            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class CommunicationThread implements Runnable{

        private Socket socket;
        private BufferedReader input;
        public CommunicationThread(Socket socket){
            this.socket = socket;
            try {
                this.input = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run(){
            while (!Thread.currentThread().isInterrupted()){
                try {
                    String read = input.readLine();
                    if (!read.equals(null))
                        handler.post(new updateUIThread("Server Said: "+read));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class updateUIThread implements Runnable{

        private String msg;

        public  updateUIThread(String msg){
            this.msg = msg;
        }

        @Override
        public void run(){
            if (!msg.equals(""))
                text.setText(msg);

        }
    }
}
