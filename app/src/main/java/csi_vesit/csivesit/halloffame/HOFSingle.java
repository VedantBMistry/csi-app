package csi_vesit.csivesit.halloffame;


public class HOFSingle {
    String event_name,name1,name2,name3,url;

    public String getEvent_name() {
        return event_name;
    }

    public String getName1() {
        return name1;
    }

    public String getName2() {
        return name2;
    }

    public String getName3() {
        return name3;
    }

    public String getUrl() {
        return url;
    }

    public HOFSingle(String event_name, String name1, String name2, String name3, String url) {

        this.event_name = event_name;
        this.name1 = name1;
        this.name2 = name2;
        this.name3 = name3;
        this.url = url;
    }
}
