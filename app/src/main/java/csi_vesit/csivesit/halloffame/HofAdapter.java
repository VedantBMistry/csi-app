package csi_vesit.csivesit.halloffame;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import csi_vesit.csivesit.R;
import csi_vesit.csivesit.events.Event;


public class HofAdapter extends ArrayAdapter {

    List list = new ArrayList();

    public HofAdapter(Context context, int resource) {
        super(context, resource);
    }


    @Override
    public void add(Object object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        HOFholder hoFholder;
        if (row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.hofrow,parent,false);
            hoFholder = new HOFholder();
            hoFholder.url = (ImageView) row.findViewById(R.id.iv);
            row.setTag(hoFholder);
        }else{
            hoFholder = (HOFholder) row.getTag();
        }
        HOFSingle hofSingle = (HOFSingle) this.getItem(position);
        Picasso.with(getContext()).load(hofSingle.getUrl()).fit().into(hoFholder.url);
        return row;
    }

    static class HOFholder{
        ImageView url;
    }

}
