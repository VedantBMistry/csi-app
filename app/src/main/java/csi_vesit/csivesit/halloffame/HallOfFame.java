package csi_vesit.csivesit.halloffame;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import csi_vesit.csivesit.Minigame.Score;
import csi_vesit.csivesit.R;

public class HallOfFame extends AppCompatActivity {

    ListView listView;
    HofAdapter hofAdapter;
    JSONObject jsonObject;
    JSONArray jsonArray;
    private Toolbar toolbar;
    ImageView imageView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hall_of_fame);

        toolbar = (Toolbar) findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);
        listView = (ListView) findViewById(R.id.listView);
        imageView3 = (ImageView) findViewById(R.id.imageView3);

        hofAdapter = new HofAdapter(this,R.layout.hofrow);
        listView.setAdapter(hofAdapter);

        imageView3.setImageResource(R.drawable.logo);
        final Animation animRotate = AnimationUtils.loadAnimation(HallOfFame.this,R.anim.rotate);
        imageView3.startAnimation(animRotate);

        Backgo backgo = new Backgo(this);
        backgo.execute();

    }

    public class Backgo extends AsyncTask<String,String,String>{


        @Override
        protected void onPostExecute(String s) {
            imageView3.clearAnimation();
            imageView3.setVisibility(View.GONE);
            try{
                imageView3.setVisibility(View.INVISIBLE);
                jsonObject = new JSONObject(s);
                jsonArray = jsonObject.getJSONArray("response");
                int count = 0;
                while (count < jsonArray.length()) {

                    JSONObject JO = jsonArray.getJSONObject(count);
                    String url = JO.getString("url");
                    String event_name = JO.getString("event_name");
                    String mem1 = JO.getString("mem1");
                    String mem2 = JO.getString("mem2");
                    String mem3 = JO.getString("mem3");

                    HOFSingle hofSingle = new HOFSingle(event_name,mem1,mem2,mem3,url);
                    hofAdapter.add(hofSingle);
                    count++;
                }

            }catch (Exception e){
                Log.i("CSIVESIT",String.valueOf(e));
                imageView3.clearAnimation();
                imageView3.setImageResource(R.drawable.no_conn);
                Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
            }
        }

        Context context;
        public Backgo(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {
            String Url = "http://businessworld.000webhostapp.com/csi/csi_hof.php";
            try {
                URL url = new URL(Url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setDoInput(true);
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String line = "";
                String result = "";
                while ((line = bufferedReader.readLine()) != null){
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
