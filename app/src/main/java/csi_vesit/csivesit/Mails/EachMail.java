package csi_vesit.csivesit.Mails;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import csi_vesit.csivesit.R;

public class EachMail extends AppCompatActivity {

    TextView tx_from,tx_sub,tx_postedOn,tx_msg;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_each_mail);

        toolbar = (Toolbar)findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String from = getIntent().getExtras().getString("mail_from");
        String date = getIntent().getExtras().getString("mail_date");
        String time = getIntent().getExtras().getString("mail_time");
        String sub = getIntent().getExtras().getString("mail_sub");
        String msg = getIntent().getExtras().getString("mail_msg");

        tx_from = (TextView)findViewById(R.id.tx_from);
        tx_sub = (TextView)findViewById(R.id.tx_sub);
        tx_postedOn = (TextView)findViewById(R.id.tx_postedOn);
        tx_msg = (TextView)findViewById(R.id.tx_msg);

        tx_from.setText(tx_from.getText().toString()+ from);
        tx_sub.setText(tx_sub.getText().toString()+ sub);
        tx_postedOn.setText(tx_postedOn.getText().toString()+ date + " at " + time);
        tx_msg.setText(msg);

    }
}
