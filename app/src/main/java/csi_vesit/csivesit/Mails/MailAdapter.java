package csi_vesit.csivesit.Mails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import csi_vesit.csivesit.R;

/**
 * Created by all on 21-Jan-17.
 */
public class MailAdapter extends ArrayAdapter{

    List list = new ArrayList();

    public MailAdapter(Context context, int resource) {
        super(context, resource);
    }

    public void add(Mail object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        MailHolder mailHolder;
        if (row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.row_layout,parent,false);
            mailHolder = new MailHolder();
            mailHolder.row_from = (TextView) row.findViewById(R.id.row_from);
            mailHolder.row_dt = (TextView) row.findViewById(R.id.row_dt);
            row.setTag(mailHolder);
        }else{
            mailHolder = (MailHolder) row.getTag();
        }
        Mail mail = (Mail) this.getItem(position);
        mailHolder.row_from.setText("From: " +mail.getFrom());
        mailHolder.row_dt.setText(mail.getD());
        return row;
    }

    static class MailHolder{
        TextView row_from,row_dt;
    }
}
