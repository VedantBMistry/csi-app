package csi_vesit.csivesit.Mails;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import csi_vesit.csivesit.R;

public class ShowMails extends AppCompatActivity {

    ListView listView;
    JSONObject jsonObject;
    JSONArray jsonArray;
    private Toolbar toolbar;
    MailAdapter mailAdapter;
    private String TAG = "CSIVESIT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_mails);

        toolbar = (Toolbar)findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            listView = (ListView) findViewById(R.id.listView);
            mailAdapter = new MailAdapter(this, R.layout.row_layout);
            listView.setAdapter(mailAdapter);

            Back2G back2g = new Back2G(this);
            back2g.execute("update");

        }catch (Exception e){
            Log.i(TAG,String.valueOf(e));
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Mail mail = (Mail) listView.getItemAtPosition(position);
                String from = mail.getFrom();
                String date = mail.getD();
                String time = mail.getT();
                String sub = mail.getSub();
                String msg = mail.getMsg();

                Intent i = new Intent(ShowMails.this, EachMail.class);
                i.putExtra("mail_from", from);
                i.putExtra("mail_date", date);
                i.putExtra("mail_time", time);
                i.putExtra("mail_sub", sub);
                i.putExtra("mail_msg", msg);
                startActivity(i);

            }
        });



    }

    public void updateList(String json_string){
        try {
            jsonObject = new JSONObject(json_string);
            jsonArray = jsonObject.getJSONArray("response");

            int count = 0;
            String from, d, t, msg,sub;
            while (count < jsonArray.length()) {
                JSONObject JO = jsonArray.getJSONObject(count);
                from = JO.getString("fromwhom");
                d = JO.getString("date");
                t = JO.getString("time");
                msg = JO.getString("msg");
                sub = JO.getString("subject");

                Mail mail = new Mail(from,d,t,msg,sub);
                mailAdapter.add(mail);
                count++;
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
            Log.i(TAG,String.valueOf(e));
        }
    }

    class Back2G extends AsyncTask<String, Void, String> {

        Context context;
        public Back2G(Context context) {
            this.context = context;
        }

        @Override
        protected void onPostExecute(String s) {
            try{
                Log.i(TAG,s);
                updateList(s);
            }catch (Exception e){
                Log.i(TAG,String.valueOf(e));
            }
        }

        @Override
        protected String doInBackground(String... params) {
            if(params[0].equals("update")){
                String Url = "http://businessworld.000webhostapp.com/myjson.php";
                try {
                    URL url = new URL(Url);
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    httpURLConnection.setDoInput(true);
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }
                    return result;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }catch (Exception e){
                    Log.i(TAG,String.valueOf(e));
                }
            }

            return null;
        }
    }


}
