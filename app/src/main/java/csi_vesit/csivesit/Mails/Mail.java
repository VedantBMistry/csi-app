package csi_vesit.csivesit.Mails;

/**
 * Created by all on 21-Jan-17.
 */
public class Mail {

    public String from,d,t,msg,sub;

    public Mail(String from, String d, String t, String msg, String sub) {
        this.from = from;
        this.d = d;
        this.t = t;
        this.msg = msg;
        this.sub = sub;
    }

    public String getFrom() {
        return from;
    }

    public String getD() {
        return d;
    }

    public String getT() {
        return t;
    }

    public String getMsg() {
        return msg;
    }

    public String getSub() {
        return sub;
    }
}
