package csi_vesit.csivesit.twitter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import csi_vesit.csivesit.R;

public class TwitterActivity extends AppCompatActivity {

    Toolbar toolbar;
    WebView web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitter);
        toolbar = (Toolbar)findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);

        web= (WebView) findViewById(R.id.webView);
        WebSettings ws=web.getSettings();
        ws.setJavaScriptEnabled(true);
        web.loadUrl("https://www.twitter.com/csi_vesit/");
        web.setWebViewClient(new WebViewClient());
    }

    @Override
    public void onBackPressed() {
        if(web.canGoBack()){
            web.goBack();
        }
        else{
            super.onBackPressed();
        }
    }
}
