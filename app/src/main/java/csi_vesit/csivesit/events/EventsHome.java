package csi_vesit.csivesit.events;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import csi_vesit.csivesit.R;

public class EventsHome extends AppCompatActivity {

    private Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    private String TAG = "CSIVESIT";
    ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_home);

        toolbar = (Toolbar)findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tabLayout = (TabLayout)findViewById(R.id.tabLayout);
        viewPager = (ViewPager)findViewById(R.id.viewPager);




        try {
            viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
            viewPagerAdapter.addFragments(new HomeFragment(), "S.E.");
            viewPagerAdapter.addFragments(new ThirdYear(), "T.E.");
            viewPagerAdapter.addFragments(new FourthYear(), "S.E. + T.E.");
            viewPagerAdapter.addFragments(new BlankFragment(), "S.E. + T.E. + B.E.");
            viewPager.setAdapter(viewPagerAdapter);
            tabLayout.setupWithViewPager(viewPager);
        }catch (Exception e) {
            Log.e(TAG, String.valueOf(e));
        }
    }
}
