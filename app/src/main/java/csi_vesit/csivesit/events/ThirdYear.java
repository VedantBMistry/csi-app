package csi_vesit.csivesit.events;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import csi_vesit.csivesit.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ThirdYear extends Fragment {

    ListView listView;
    EventAdapter eventAdapter;

    public ThirdYear() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_third_year, container, false);

        listView = (ListView)view.findViewById(R.id.listView);
        eventAdapter = new EventAdapter(getContext(),R.layout.events_row);


        Event event = new Event(R.drawable.cb,"Code Breaker");
        eventAdapter.add(event);

        event = new Event(R.drawable.php_mysql , "Laravel Workshop");
        eventAdapter.add(event);
        listView.setAdapter(eventAdapter);
        return view;
    }

}
