package csi_vesit.csivesit.events;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import csi_vesit.csivesit.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    ListView listView;
    EventAdapter eventAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container,false);
        // Inflate the layout for this fragment
        listView = (ListView)view.findViewById(R.id.listView);
        eventAdapter = new EventAdapter(getContext(),R.layout.events_row);


        Event event = new Event(R.drawable.hd,"Hidden Cipher");
        eventAdapter.add(event);

        event = new Event(R.drawable.wordpress , "Wordpress Workshop");
        eventAdapter.add(event);

        event = new Event(R.drawable.javafx , "Java Fx Workshop");
        eventAdapter.add(event);

        event = new Event(R.drawable.php_mysql , "PHP + MySQL Workshop");
        eventAdapter.add(event);
        listView.setAdapter(eventAdapter);

        return view;
    }

}
