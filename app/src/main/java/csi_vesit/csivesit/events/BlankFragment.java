package csi_vesit.csivesit.events;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import csi_vesit.csivesit.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {

    ListView listView;
    EventAdapter eventAdapter;
    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_third_year, container, false);
        listView = (ListView)view.findViewById(R.id.listView);
        eventAdapter = new EventAdapter(getContext(),R.layout.events_row);

        Event event = new Event(R.drawable.cashin , "Cash in");
        eventAdapter.add(event);

        event = new Event(R.drawable.cricorecent , "Cric-O-Mania");
        eventAdapter.add(event);

        event = new Event(R.drawable.vsm , "V.S.M");
        eventAdapter.add(event);

        event = new Event(R.drawable.lannew , "Lan Gaming");
        eventAdapter.add(event);
        event = new Event(R.drawable.idiotbox , "Idiot Box");
        eventAdapter.add(event);
        listView.setAdapter(eventAdapter);
        return view;
    }

}
