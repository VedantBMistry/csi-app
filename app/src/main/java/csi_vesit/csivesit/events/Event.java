package csi_vesit.csivesit.events;

/**
 * Created by all on 02-Feb-17.
 */
public class Event {

    int id;
    String name;

    public Event(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
