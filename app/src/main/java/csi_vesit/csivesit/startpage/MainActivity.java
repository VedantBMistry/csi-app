package csi_vesit.csivesit.startpage;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;
import com.squareup.picasso.Transformation;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import csi_vesit.csivesit.Buzzer.buzzhome;
import csi_vesit.csivesit.CommityMembers.ComMembers;
import csi_vesit.csivesit.CommityMembers.DatabaseOpener;
import csi_vesit.csivesit.InstagramPosts.InstaActivity;
import csi_vesit.csivesit.Mails.ShowMails;
import csi_vesit.csivesit.Minigame.Auth;
import csi_vesit.csivesit.MySingleton;
import csi_vesit.csivesit.R;
import csi_vesit.csivesit.aboutus.boutUs;
import csi_vesit.csivesit.events.EventsHome;
import csi_vesit.csivesit.facebook.FacebookHost;
import csi_vesit.csivesit.feedback.FeedbackPost;
import csi_vesit.csivesit.halloffame.HallOfFame;
import csi_vesit.csivesit.twitter.TwitterActivity;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    ViewPager switcher;
    private Toolbar toolbar;
    DatabaseOpener myDb;
    NavigationView navigationView;
    CustomAdapter customAdapter;
    ImageView scored,buzzered,show_mai,compose,hof,commemb,facebook_button,insta_button,about_us_btn;
    private String TAG = "CSIVESIT";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar)findViewById(R.id.nav_action);
        setSupportActionBar(toolbar);
        Log.i(TAG,"hello0");
        myDb = new DatabaseOpener(this);
        Log.i(TAG,"hello4");



        switcher = (ViewPager)findViewById(R.id.switcher);
        facebook_button = (ImageView) findViewById(R.id.facebook_button);
        insta_button = (ImageView) findViewById(R.id.insta_button);
        about_us_btn = (ImageView) findViewById(R.id.abt_us_btn);
        scored = (ImageView) findViewById(R.id.score);
        commemb = (ImageView) findViewById(R.id.commemb);
        buzzered = (ImageView) findViewById(R.id.buzzer);
        hof = (ImageView) findViewById(R.id.hof2);
        show_mai = (ImageView) findViewById(R.id.show_mai);
        compose = (ImageView) findViewById(R.id.compose);

        customAdapter = new CustomAdapter(this);
        switcher.setAdapter(customAdapter);


        about_us_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animator animator = AnimatorInflater.loadAnimator(MainActivity.this,R.animator.fade);
                animator.setTarget(v);
                animator.start();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(MainActivity.this, boutUs.class);
                startActivity(i);
            }
        });


        insta_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animator animator = AnimatorInflater.loadAnimator(MainActivity.this,R.animator.fade);
                animator.setTarget(v);
                animator.start();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(MainActivity.this, InstaActivity.class);
                startActivity(i);
            }
        });


        commemb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animator animator = AnimatorInflater.loadAnimator(MainActivity.this,R.animator.fade);
                animator.setTarget(v);
                animator.start();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(MainActivity.this,ComMembers.class);
                startActivity(i);
            }
        });


        facebook_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animator animator = AnimatorInflater.loadAnimator(MainActivity.this,R.animator.fade);
                animator.setTarget(v);
                animator.start();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(MainActivity.this, FacebookHost.class);
                startActivity(i);
            }
        });

        compose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animator animator = AnimatorInflater.loadAnimator(MainActivity.this,R.animator.fade);
                animator.setTarget(v);
                animator.start();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(MainActivity.this, FeedbackPost.class);
                startActivity(i);
            }
        });

        hof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animator animator = AnimatorInflater.loadAnimator(MainActivity.this,R.animator.fade);
                animator.setTarget(v);
                animator.start();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Intent i = new Intent(MainActivity.this, HallOfFame.class);
                startActivity(i);
            }
        });

        show_mai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animator animator = AnimatorInflater.loadAnimator(MainActivity.this,R.animator.fade);
                animator.setTarget(v);
                animator.start();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(MainActivity.this, ShowMails.class);
                startActivity(i);
            }
        });

        scored.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animator animator = AnimatorInflater.loadAnimator(MainActivity.this,R.animator.fade);
                animator.setTarget(v);
                animator.start();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(MainActivity.this,Auth.class);
                startActivity(i);
            }
        });

        buzzered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animator animator = AnimatorInflater.loadAnimator(MainActivity.this,R.animator.fade);
                animator.setTarget(v);
                animator.start();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(MainActivity.this,buzzhome.class);
                startActivity(i);
            }
        });

        about_us_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animator animator = AnimatorInflater.loadAnimator(MainActivity.this,R.animator.fade);
                animator.setTarget(v);
                animator.start();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(MainActivity.this,TwitterActivity.class);
                startActivity(i);
            }
        });

        switcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animator animator = AnimatorInflater.loadAnimator(MainActivity.this,R.animator.fade);
                animator.setTarget(v);
                animator.start();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(MainActivity.this,EventsHome.class);
                startActivity(i);
            }
        });

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimer(),5000,2000);



        drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,R.string.Open,R.string.Close);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();


        {
            Cursor res = myDb.showMember();
            if (res.getCount() == 0) {
                int id = R.drawable.chirag;
                boolean up = myDb.insertMember("Mr. Chirag Makhija", "CSI Correspondent", id);
                id = R.drawable.vickey;
                up = myDb.insertMember("Mr. Vickey Melwani", "CSI Correspondent", id);
                id = R.drawable.sneha;
                up = myDb.insertMember("Ms. Sneha Binani", "Executive Committee", id);
                id = R.drawable.deepika_khatri;
                up = myDb.insertMember("Ms. Deepika Khatri", "Executive Committee", id);
                id = R.drawable.arjun;
                up = myDb.insertMember("Mr. Arjun Bajpai", "Executive Committee", id);
                id = R.drawable.meghna;
                up = myDb.insertMember("Ms. Meghana Peswani", "Executive Committee", id);
                id = R.drawable.kriti;
                up = myDb.insertMember("Ms. Kriti Anant", "Executive Committee", id);
                id = R.drawable.shubham;
                up = myDb.insertMember("Mr. Shubham Koyande", "Executive Committee", id);

                id = R.drawable.pranay_mahaldar;
                up = myDb.insertMember("Mr. Pranay Mahaldar", "ChairPerson", id);

                id = R.drawable.shantanu_shende;
                up = myDb.insertMember("Mr. Shantanu Shende", "Co-ChairPerson", id);
                id = R.drawable.adityakrishnan;
                up = myDb.insertMember("Mr. Aditya Krishnan", "Secretary", id);
                id = R.drawable.ashwini;
                up = myDb.insertMember("Ms. Ashwini Vedula", "Treasurer", id);

                id = R.drawable.sumeetshahani;
                up = myDb.insertMember("Mr. Sumeet Shahani", "Sr. Technical Officer", id);
                id = R.drawable.juhibhagtani;
                up = myDb.insertMember("Ms. Juhi Bhagtani", "Sr. Technical Officer", id);
                id = R.drawable.siddheshhindalekar;
                up = myDb.insertMember("Mr. Siddhesh Hindalekar", "Sr. Technical Officer", id);

                id = R.drawable.tanmayrauth2;
                up = myDb.insertMember("Mr. Tanmay Rauth", "Sr. Web Editor", id);
                id = R.drawable.neeraj;
                up = myDb.insertMember("Mr. Neeraj Jethnani", "Sr. Web Editor", id);
                id = R.drawable.yashchetnani;
                up = myDb.insertMember("Mr. Yash Chetnani", "Sr. Operations Officer", id);
                id = R.drawable.samdavid;
                up = myDb.insertMember("Mr. Sam David", "Sr. Operations Officer", id);
                id = R.drawable.rohanmohandas;
                up = myDb.insertMember("Mr. Rohan Mohandas", "Sr. Operations Officer", id);
                id = R.drawable.shrutishetty;
                up = myDb.insertMember("Ms. Shruti Shetty", "Sr. Operations Officer", id);
                id = R.drawable.mihirwagle;
                up = myDb.insertMember("Mr. Mihir Wagle", "Sr. Editor", id);
                id = R.drawable.sejalgianani;
                up = myDb.insertMember("Ms. Sejal Gianani", "Sr. Editor", id);
                id = R.drawable.rohitgarg;
                up = myDb.insertMember("Mr. Rohit Garg", "Sr. Public Relation Officer", id);
                id = R.drawable.poojabhatia;
                up = myDb.insertMember("Ms. Pooja Bhatia", "Sr. Public Relation Officer", id);
                id = R.drawable.jayeshnagpal;
                up = myDb.insertMember("Mr. Jayesh Nagpal", "Sr. Public Relation Officer", id);


                id = R.drawable.kaushalmhalgi;
                up = myDb.insertMember("Mr. Kaushal Mhalgi", "Jr. Technical Officer", id);

                id = R.drawable.akshayapatil;
                up = myDb.insertMember("Ms. Akshaya Patil", "Jr. Technical Officer", id);

                id = R.drawable.vedantmistry;
                up = myDb.insertMember("Mr. Vedant Mistry", "Jr. Technical Officer", id);

                id = R.drawable.arvindnarayanan;
                up = myDb.insertMember("Mr. Arvind Narayanan", "Jr. Web Editor", id);

                id = R.drawable.binoysaha;
                up = myDb.insertMember("Mr. Binoy Saha", "Jr. Web Editor", id);

                id = R.drawable.manasikhamkar;
                up = myDb.insertMember("Ms. Manasi Khamkar", "Jr. Web Editor", id);

                id = R.drawable.siddhantyeole;
                up = myDb.insertMember("Mr. Siddhant Yeole", "Jr. Operations Officer", id);

                id = R.drawable.adarshshetty;
                up = myDb.insertMember("Mr. Adarsh Shetty", "Jr. Operations Officer", id);
                id = R.drawable.sanjayudasi;
                up = myDb.insertMember("Mr. Sanjay Udasi", "Jr. Operations Officer", id);
                id = R.drawable.surajbhat;
                up = myDb.insertMember("Mr. Suraj Bhat", "Jr. Operations Officer", id);
                id = R.drawable.jagritbhat;
                up = myDb.insertMember("Mr. Jagrit Bhat", "Jr. Operations Officer", id);
                id = R.drawable.prathameshwagh;
                up = myDb.insertMember("Mr. Prathamesh Wagh", "Jr. Operations Officer", id);
                id = R.drawable.ashwinsoni;
                up = myDb.insertMember("Mr. Ashwin Soni", "Jr. Operations Officer", id);
                id = R.drawable.rushikeshgawande;
                up = myDb.insertMember("Mr. Rushikesh Gawande", "Jr. Operations Officer", id);
                id = R.drawable.rohan_keswani;
                up = myDb.insertMember("Mr. Rohan Keswani", "Jr. Public Relation Officer", id);
                id = R.drawable.reshmasawlani;
                up = myDb.insertMember("Ms. Reshma Sawlani", "Jr. Public Relation Officer", id);
                id = R.drawable.saahilhiranandani;
                up = myDb.insertMember("Mr. Saahil Hiranandani", "Jr. Public Relation Officer", id);
                id = R.drawable.trenadhingra;
                up = myDb.insertMember("Ms. Trena Dhingra", "Jr. Public Relation Officer", id);
                id = R.drawable.utsavdas;
                up = myDb.insertMember("Mr. Utsav Das", "Jr. Editor", id);
                id = R.drawable.shaunakbaaradkar;
                up = myDb.insertMember("Mr. Shaunak Baaradkar", "Jr. Editor", id);


            }
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigationView = (NavigationView)findViewById(R.id.nav_view);

        switcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animator animator = AnimatorInflater.loadAnimator(MainActivity.this,R.animator.fade);
                animator.setTarget(v);
                animator.start();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(MainActivity.this, EventsHome.class);
                startActivity(i);
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                switch (item.getItemId()){
                    case R.id.committee_members:
                        Intent i = new Intent(MainActivity.this, ComMembers.class);
                        startActivity(i);
                        break;

                    case R.id.hall_of_fame:
                        Intent i2 = new Intent(MainActivity.this, HallOfFame.class);
                        startActivity(i2);
                        break;

                    case R.id.facebook2:
                        Intent i3 = new Intent(MainActivity.this, FacebookHost.class);
                        startActivity(i3);
                        break;

                    case R.id.insta2:
                        Intent i4 = new Intent(MainActivity.this, InstaActivity.class);
                        startActivity(i4);
                        break;

                    case R.id.nav_about_us:
                        Intent i5 = new Intent(MainActivity.this, boutUs.class);
                        startActivity(i5);
                        break;

                    case R.id.feedback:
                        Intent in = new Intent(MainActivity.this, FeedbackPost.class);
                        startActivity(in);
                        break;

                    case R.id.nav_buzzer:
                        Intent inte = new Intent(MainActivity.this, buzzhome.class);
                        startActivity(inte);
                        break;

                    case R.id.nav_scores:
                        Intent inten = new Intent(MainActivity.this, Auth.class);
                        startActivity(inten);
                        break;

                    case R.id.nav_show_mails:
                        Intent intent = new Intent(MainActivity.this, ShowMails.class);
                        startActivity(intent);
                        break;

                    case R.id.nav_events:
                        Intent intent2 = new Intent(MainActivity.this, EventsHome.class);
                        startActivity(intent2);
                        break;
                }
                return false;
            }
        });


        Cursor Enter = myDb.showEntry();
        if(Enter.getCount() == 0) {
            SharedPreferences sharedPef = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
            final String token = sharedPef.getString(getString(R.string.FCM_TOKEN), "");
            StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.POST, "http://192.168.1.103/CsiScore/fcm_insrt.php",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(response.equals("Successful")){
                                myDb.insertEntry(1);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("token", token);
                    Log.i(TAG, token);
                    return params;
                }
            };
            MySingleton.getmInstance(this).addToRequestQue(stringRequest);

        }

    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class MyTimer extends TimerTask{

        @Override
        public void run() {

            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(switcher.getCurrentItem()==0){
                        switcher.setCurrentItem(1);
                    }else if(switcher.getCurrentItem()==1){
                        switcher.setCurrentItem(2);
                    }else if(switcher.getCurrentItem()==2){
                        switcher.setCurrentItem(3);
                    }else if(switcher.getCurrentItem()==3){
                        switcher.setCurrentItem(0);
                    }

                }
            });


        }
    }



}
