package csi_vesit.csivesit.startpage;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import csi_vesit.csivesit.R;
import csi_vesit.csivesit.events.EventsHome;

/**
 * Created by all on 22-Jan-17.
 */
public class CustomAdapter extends PagerAdapter {

    private int[] image_resources = {R.drawable.cynosure,R.drawable.cricomania,R.drawable.lan_gaming,R.drawable.idiotbox};
    private String[] imageURL = {"https://businessworld.000webhostapp.com/csi/sympo.jpg",
            "https://businessworld.000webhostapp.com/csi/crico.png",
            "https://businessworld.000webhostapp.com/csi/lan.png",
            "https://businessworld.000webhostapp.com/csi/idiotbox.png"};
    private Context ctx;
    private LayoutInflater layoutInflater;

    public CustomAdapter(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return image_resources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==(LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.swipe_layout,container,false);
        ImageView imageView = (ImageView)view.findViewById(R.id.swipe_image);
        TextView text_event = (TextView)view.findViewById(R.id.text_event);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ctx, EventsHome.class);
                ctx.startActivity(i);
            }
        });


        Picasso.with(ctx).load(imageURL[position]).fit().error(image_resources[position]).placeholder(image_resources[position]).into(imageView);
        //imageView.setImageResource(image_resources[position]);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
